<!-- META PROPERTIES -->
    
    <!-- Bootstrap meta settings -->
    <meta charset="utf-8" />
    <base href="/">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Personal meta settings -->
	<meta name="description" content="">
    <link rel="icon" type="image/jpeg" href="img/reSolveFavicon.png"/>
    <title>reSolve</title>

	<!-- SEO -->
	<meta name="fragment" content="!">

    <!-- FB -->
	<meta property="og:image" content="" />
    <meta property="og:description" content=""/>
    <meta property="og:url" content="http://resolve.reseed.it" />
    <meta property="og:title" content="reSolve" />
            
<!-- BOOTSTRAP -->
    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    
    <!-- FONT-AWESOME -->
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.css" />
    
    <!-- GOOGLE FONTS -->
    <link href='https://fonts.googleapis.com/css?family=Raleway:700,400,300' rel='stylesheet' type='text/css' />

    
<!-- JQUERY FOR BOOTSTRAP -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.js"></script>
    
<!-- CSS -->
	
	<link rel="stylesheet" href="css/reSolve.css" />



    
    