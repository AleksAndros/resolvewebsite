<!DOCTYPE html>
<html lang="it" xmlns="http://www.w3.org/1999/xhtml"	xmlns:fb="http://ogp.me/ns/fb#">

<head>
<!-- HEAD DECLARATION -->
    <?php include_once("head.php"); ?>
</head>

<body>

<div class="col-xs-12">
	<div class="row">	
	
		<img id="hand-desktop" src="/img/hand.png" class="hidden-xs hidden-sm" />
		
		<div class="col-md-12 hidden-xs hidden-sm background-img">
			<div class="row">
			  		<div class="col-md-4">
			  		</div>
			  		<div class="col-md-8">
					  		<div class="row">
					  
								  <div class="col-md-3">
								  </div>
								  
								  <div class="col-md-9">
									  	<div class="row">
									  		<div id="banner" class="col-md-12">
									  		
												<div class="row">
													<div class="col-md-6">
														<img id="logo" class="img-responsive" alt="reSolve" src="img/logored.png" />
													</div>
													<div class="col-md-6">
													</div>
												</div>
												
												<div class="row">
													<div class="col-md-12">
														<h2 class="text-justify white">
															<b>Inserisci</b> qualsiasi esercizio di Matematica. <br />
															<b>Verifica</b> la soluzione step-by-step.
														</h2>
													</div>
												</div>
												
												<div class="row">
													<div class="col-md-6">
														<a href="https://play.google.com/apps/testing/it.reseed.flati.resolve" ><img class="img-responsive" alt="reSolve su Google Play" src="img/google-play-badge.png" /></a>
													</div>
													<div class="col-md-4">
														<a href="https://www.facebook.com/resolveapplication/"><i class="fa fa-12x fa-facebook-square white"></i></a>
													</div>
													<div class="col-md-2">
													</div>
												</div>
												
											</div>
										</div>
								  	</div>
								  	
					  		</div>
				  	
						  	<div id="tabs" class="row with-a-little-margin">
								<div class="col-md-4 text-center red">
									<i class="fa fa-eur fa-4x" aria-hidden="true"></i>
									<h3>GRATIS</h3>
									<p class="white with-margin text-justify"><b>reSolve è completamente gratuita. Devi solo installarla sul tuo smartphone Android e cominciare a risolvere esercizi. <br/> Approfittane subito!</b></p>
								</div>
								<div class="col-md-4 text-center red">
									<i class="fa fa-check-circle fa-4x" aria-hidden="true"></i>
									<h3>SOLUZIONE STEP-BY-STEP</h3>
									<p class="white with-margin text-justify"><b>reSolve dà soluzioni dettagliate con tutti i singoli passaggi matematici spiegati accuratamente e presentati come richiesto a scuola.</b></p>
								</div>
								<div class="col-md-4 text-center red">
									<i class="fa fa-flag fa-4x" aria-hidden="true"></i>
									<h3>TOTALMENTE IN ITALIANO</h3>
									<p class="white with-margin text-justify"><b>Non solo reSolve ha come lingua principale l'Italiano, ma è stata sviluppata in base ai programmi di matematica delle scuole superiori italiane.</b></p>
								</div>
						  	</div>
						  	
<!-- 							  	<div class="row with-a-little-margin"> -->
<!-- 									<div class="col-md-4 text-center red"> -->
									
<!-- 									</div> -->
<!-- 									<div class="col-md-4 text-center red"> -->
									
<!-- 									</div> -->
<!-- 									<div class="col-md-4 text-center red"> -->
									
<!-- 									</div> -->
<!-- 							  	</div> -->

							<hr>

							<div id="questions" class="row with-a-little-margin">
								
								<div id="doyouwantdesktop" class="col-md-12">
									
									<ul>
										<li class="white"><b>Vuoi verificare se hai svolto bene il <span class="red">compito in classe</span>?</b></li>
										<li class="white"><b>L'esercizio che hai appena fatto non ha la <span class="red">soluzione</span>?</b></li>
										<li class="white"><b>Sospetti che la <span class="red">soluzione del libro</span> non sia corretta?</b></li>
										<li class="white"><b>Vuoi vedere il <span class="red">grafico di una funzione</span>?</b></li>
									</ul>
									
								</div>
								
<!-- 									<div class="col-md-12"> -->
<!-- 										<a href="https://play.google.com/apps/testing/it.reseed.flati.resolve" ><img class="img-responsive center-block" alt="reSolve su Google Play" src="img/google-play-badge.png" /></a> -->
<!-- 									</div> -->
								
								<div class="col-md-12 white description">
									reSolve è l'app per smartphone e tablet che permette di risolvere ogni esercizio di matematica con lo svolgimento mostrato step-by-step. <br/>
									Equazioni e disequazioni di ogni grado e parametriche, limiti, integrali, derivate, studi di funzione e ogni altro tipo di esercizio può essere inserito in reSolve non solo per verificarne la soluzione, ma per apprenderne lo svolgimento mostrando i singoli passi e le relative spiegazioni. 
								</div>
								
							</div>
						  	
				  		</div>
				  	
				  </div>
			</div>
		</div>
	
		<!--  MOBILE  -->
		<div class="col-xs-12 visible-xs visible-sm no-padding">
			<div class="row">
				<div id="banner" class="col-xs-12 background-red">
					<div class="row">
						<div class="col-xs-2 col-sm-3 col-md-4">
						</div>
						<div class="col-xs-8 col-sm-6 col-md-4">
							<img id="logo" class="img-responsive center-block" alt="reSolve" src="img/logo.png" />
						</div>
						<div class="col-xs-2 col-sm-3 col-md-4">
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<h5 class="text-center white">
								<b>Inserisci</b> qualsiasi esercizio di Matematica. <br />
								<b>Verifica</b> la soluzione step-by-step.
							</h5>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-xs-8 social">
					<a href="https://play.google.com/apps/testing/it.reseed.flati.resolve" ><img class="img-responsive center-block google-play-badge" alt="reSolve su Google Play" src="img/google-play-badge.png" /></a>
				</div>
				<div class="col-xs-4 social">
					<a href="https://www.facebook.com/resolveapplication/"><img class="img-responsive center-block facebook-badge" alt="reSolve su Facebook" src="img/facebook-badge.png" /></a>				
				</div>	
			</div>
			
			<div class="row">
				<div id="hand" class="col-xs-12">
					<img id="hand-img" class="img-responsive" alt="reSolve in mano" src="/img/hand.png" />
				</div>
			</div>
			
			<div class="row">
				<div id="presentation" class="col-xs-12 background-red">
					<div id="gratis" class="row">
						<div class="col-xs-12 text-center black">
							<i class="fa fa-eur fa-4x" aria-hidden="true"></i>
							<h2>GRATIS</h2>
							<p class="white with-margin text-justify"><b>reSolve è completamente gratuita. Devi solo installarla sul tuo smartphone Android e cominciare a risolvere esercizi. <br/> Approfittane subito!</b></p>
						</div>
					</div>
					<div id="step-by-step" class="row">
						<div class="col-xs-12 text-center black">
							<i class="fa fa-check-circle fa-4x" aria-hidden="true"></i>
							<h2>SOLUZIONE STEP-BY-STEP</h2>
							<p class="white with-margin text-justify"><b>reSolve dà soluzioni dettagliate con tutti i singoli passaggi matematici spiegati accuratamente e presentati come richiesto a scuola.</b></p>
						</div>			
					</div>
					<div id="italiano" class="row">
						<div class="col-xs-12 text-center black">
							<i class="fa fa-flag fa-4x" aria-hidden="true"></i>
							<h2>TOTALMENTE IN ITALIANO</h2>
							<p class="white with-margin text-justify"><b>Non solo reSolve ha come lingua principale l'Italiano, ma è stata sviluppata in base ai programmi di matematica delle scuole superiori italiane.</b></p>
						</div>			
					</div>
				</div>
			</div>
			
			<div class="row">
				<div id="doyouwant" class="col-xs-12 with-margin with-vertical-padding">
					<ul>
						<li class="black"><b>Vuoi verificare se hai svolto bene il <span class="red">compito in classe</span>?</b></li>
						<li class="black"><b>L'esercizio che hai appena fatto non ha la <span class="red">soluzione</span>?</b></li>
						<li class="black"><b>Sospetti che la <span class="red">soluzione del libro</span> non sia corretta?</b></li>
						<li class="black"><b>Vuoi vedere il <span class="red">grafico di una funzione</span>?</b></li>
					</ul>
				</div>
			</div>
			
			<div class="row">
				<div class="col-xs-8 social">
					<img class="img-responsive center-block google-play-badge" alt="reSolve su Google Play" src="img/google-play-badge.png" />
				</div>
				<div class="col-xs-4 social">
					<img class="img-responsive center-block facebook-badge" alt="reSolve su Facebook" src="img/facebook-badge.png" />				
				</div>	
			</div>
			
			<div class="row">
				<div class="col-xs-12 with-vertical-padding text-center background-green">
					<h3 id="powered" class="white">by reSeed</h3>
				</div>
			</div>
		</div>
</div>
	
</body>
	
</html>